var script = d3.select('#gallery-script');

// ************************
// Constants and arguments
// ************************

// Get the arguments passed from front-matter
const args = JSON.parse(script.attr("args"));

const width = args.width; // plot area width, without padding/margins
const height = args.height;
const padding = args.padding;
const band = args.band;

// Set other constants, like min an max ranges
const range_x = 10;
const step_x = 0.05;
const max_y = 0.8;
const range_mu = 3;
const step_mu = 0.1;
const min_sigma = 0.5;
const max_sigma = 3;
const step_sigma = 0.1;
const types = ["pdf", "cdf"];
var transition_mode = d3.easeLinear;
const auc_line_width = 16;

// Gallery container
const gallery = d3.select("#gallery-container");
// derived constants
const max_x = 0 + range_x;
const min_x = 0 - range_x;
const min_mu = 0 - range_mu;
const max_mu = 0 + range_mu;
// single plot dimensions
const plot_width = padding.left + width + padding.right;
const plot_height = {"pdf": padding.top + height + padding.between/2,
                     "cdf": padding.between/2 + height + padding.bottom};
const abs_band = {"bottom": band.bottom*padding.between/2,
                 "top": band.top*padding.between/2};
const label_position_y = plot_height.pdf + (abs_band.bottom -abs_band.top)/2;
const label_offset_x = 0;

// **********
// Functions
// **********

// Define PDF for normal distribution
function dnorm(x, mu, sigma) {
  sigma_sq = Math.pow(sigma, 2); // compute sigma^2
  p = 1. / Math.sqrt(2 * Math.PI * sigma_sq) * Math.exp(-1./(2 * sigma_sq) * Math.pow(x-mu, 2));
  return(p)
}

// Define CDF for normal distribution
function pnorm(x, mu, sigma) {
  sigma_sq = Math.pow(sigma, 2); // compute sigma^2
  Prob = 1/2 * (1 + math.erf((x - mu)/Math.sqrt(2 * sigma_sq)))
  return(Prob)
}

// auxilliary function to fill dataset
function fill_dataset(x, y, z) {
  // if the first and the last element need to be repeated, the dataset already contains some
  // datapoints at this stage (= shift)
  // x: x coordinates
  // y: y coordinates for the pdf plot
  // z: y coordinates for the cdf plot

  var filled_dataset = []
  for (var j = 0; j < x.length; j++) {
    filled_dataset[j] = []
    filled_dataset[j][0] = x[j]
    filled_dataset[j][1] = y[j]
    filled_dataset[j][2] = z[j]
  }
  return(filled_dataset)
}

// Calculate the curve coordinates
function get_data_for_curves(x, mu, sigma) {
  // x vector
  // y = PDF, z = CDF
  var y = new Array()
  var z = new Array()
  for (var i = 0 ; i < x.length ; i++) {
    y[i] = dnorm(x[i], mu, sigma)
    z[i] = pnorm(x[i], mu, sigma)
  }

  dataset = fill_dataset(x, y, z)

  return(dataset)
}

// Calculate AUC coordinates
function get_coordinates_for_AUC(mu, sigma) {
  var AUC_coordinates = {"pdf": {"sigma_AUC": {"x_min": mu - sigma, "x_max": mu + sigma,
                                               "y_min": 0, "y_max": max_y}}};
  return(AUC_coordinates)
}

// Calculate datasets for clip paths
function get_data_for_clip_paths(data) {

  var clip_path_data_exact_below = [[data[0][0], 0, 0]].concat(data).concat([[data[data.length-1][0], 0, 0]]);
  var clip_path_data_w_band = [[data[0][0], 0 - abs_band.bottom/height, 1 + abs_band.top/height]].concat(data).concat([[data[data.length-1][0], 0 - abs_band.bottom/height, 1 + abs_band.top/height]]);
  var clip_path_data = {"exact_clip_path": clip_path_data_exact_below,
                        "band_clip_path": clip_path_data_w_band};
  return(clip_path_data);
}

// Calculate coordinates for straight lines
function get_coordinates_for_straight_lines(mu, sigma) {
    var straight_line_coordinates = {"pdf": {"sigma_left_aux_line": {"x1": mu - sigma, "x2": mu - sigma,
                                                                         "y1": 0 - abs_band.bottom/height, "y2": max_y},
                                             "sigma_right_aux_line": {"x1": mu + sigma, "x2": mu + sigma,
                                                                          "y1": 0 - abs_band.bottom/height, "y2": max_y},
                                             "mu_aux_line": {"x1": mu, "x2": mu,
                                                                 "y1": 0 - abs_band.bottom/height, "y2": max_y},
                                             "sigma_left_main_line": {"x1": mu - sigma, "x2": mu - sigma,
                                                                      "y1": 0, "y2": max_y},
                                             "sigma_right_main_line": {"x1": mu + sigma, "x2": mu + sigma,
                                                                       "y1": 0, "y2": max_y},
                                             "mu_main_line": {"x1": mu, "x2": mu,
                                                              "y1": 0, "y2": max_y}},
                                     "cdf": {"sigma_left_aux_line": {"x1": mu - sigma, "x2": mu - sigma,
                                                                         "y1": 0, "y2": 1 + abs_band.top/height},
                                             "sigma_right_aux_line": {"x1": mu + sigma, "x2": mu + sigma,
                                                                          "y1": 0, "y2": 1 + abs_band.top/height},
                                             "horizontal_sigma_left_aux_line": {"x1": min_x, "x2": max_x,
                                                                                    "y1": pnorm(mu - sigma, mu, sigma), "y2": pnorm(mu - sigma, mu, sigma)},
                                             "horizontal_sigma_right_aux_line": {"x1": min_x, "x2": max_x,
                                                                                     "y1": pnorm(mu + sigma, mu, sigma), "y2": pnorm(mu + sigma, mu, sigma)},
                                             "horizontal_mu_aux_line": {"x1": min_x, "x2": max_x,
                                                                            "y1": pnorm(mu, mu, sigma), "y2": pnorm(mu, mu, sigma)},
                                             "mu_aux_line": {"x1": mu, "x2": mu,
                                                                 "y1": 0, "y2": 1 + abs_band.top/height},
                                             "sigma_left_main_line": {"x1": mu - sigma, "x2": mu - sigma,
                                                                      "y1": 0, "y2": 1},
                                             "sigma_right_main_line": {"x1": mu + sigma, "x2": mu + sigma,
                                                                       "y1": 0, "y2": 1},
                                             "mu_main_line": {"x1": mu, "x2": mu,
                                                              "y1": 0, "y2": 1},
                                             "sigma_AUC_line": {"x1": min_x + auc_line_width/2/width*(max_x - min_x), "x2": min_x + auc_line_width/2/width*(max_x - min_x),
                                                                "y1": pnorm(mu - sigma, mu, sigma), "y2": pnorm(mu + sigma, mu, sigma)}}};
    return(straight_line_coordinates);
}

function get_data(x, mu, sigma) {
  var curve_data = get_data_for_curves(x, mu, sigma);
  var clip_path_data = get_data_for_clip_paths(curve_data);
  var AUC_coordinates = get_coordinates_for_AUC(mu, sigma);
  var straight_line_coordinates = get_coordinates_for_straight_lines(mu, sigma);
  return({"curve": curve_data,
          "clip_path": clip_path_data,
          "AUC": AUC_coordinates,
          "straight_line": straight_line_coordinates});
}

// Define how the slider values should be shown
function repr_value(x) {
  return(parseFloat(x).toFixed(1))
}

// Define transition duration
function transition_duration(delta_mu, delta_sigma) {
  var duration_mu = 100 + delta_mu*100
  var duration_sigma = 100 + delta_sigma*200
  return(duration_mu + duration_sigma)
}

// ***************
// Input: sliders
// ***************

// Add a slider to the gallery-container
const sliders = gallery.append("form")
  .attr("class", "gaussian-params");
// Add title
sliders.append("h5")
  .attr("class", "gaussian-params-title")
  .html("Parameters");

// Method to append a slider
sliders.append_slider = function(param_name, param_label, initial_value, min_value, max_value, step) {

  div = this.append("div")
    .attr("class", "slider-container");
  div.append("label")
    .attr("for", `${param_name}-slider`)
    .attr("class", "form-label slider-label")
    .html(param_label);
  value = div.append("output")
    .attr("id", `${param_name}-slider-value`)
    .attr("class", "slider-value");
  slider = div.append("input")
    .attr("id", `${param_name}-slider`)
    .attr("class", "form-range slider-range")
    .attr("type", "range")
    .attr("min", min_value)
    .attr("max", max_value)
    .attr("step", step)
    .attr("value", initial_value);
  // Show initial value of the slider in the output
  value.html(repr_value(slider.attr("value")));

  return({"div": div,
          "value": value,
          "slider": slider});
}

// Add mu-slider
var slider_mu = sliders.append_slider("mu", "Mean $\\mu \\in \\mathbb{R}$:", 0, min_mu, max_mu, step_mu);
// Add sigma-slider
var slider_sigma = sliders.append_slider("sigma", "Standard deviation $\\sigma \\in \\mathbb{R}^{+}$:", 1, min_sigma, max_sigma, step_sigma);

// Get current mu and sigma methods
slider_mu.mu = function() {
  return (parseFloat(this.slider.attr("value")))
}
slider_sigma.sigma = function() {
  return (parseFloat(this.slider.attr("value")))
}

// *******
// Canvas
// *******

// Append SVG to the gallery-container
const svg = gallery.append("div")
    .attr("id", "canvas-container")
  .append("svg")
    .attr("id", "canvas")
    .attr("width", plot_width)
    .attr("height", plot_height.pdf + plot_height.cdf);

// Add background to the plots
svg.add_background = function(y_shift) {
  this.append("rect")
    .attr("width", width)
    .attr("height", height)
    .attr("x", padding.left)
    .attr("y", y_shift)
    .attr("fill", args.background);
}
svg.add_background(padding.top); // upper plot - PDF
svg.add_background(padding.between/2 + plot_height.pdf); // lower plot - CDF

// *******
// Scales
// *******

// x-scale
var xScale = d3.scaleLinear()
  .domain([min_x, max_x])
  .range([0, width]);
// y-scales
var yScale = {"pdf": d3.scaleLinear()
                      .domain([0, max_y])
                      .range([height, 0]),
              "cdf": d3.scaleLinear()
                      .domain([0, 1])
                      .range([height, 0])};

// oversize y-scales
var oversize_yScale = {"pdf": d3.scaleLinear()
                                .domain([0 - abs_band.bottom*max_y/height, max_y])
                                .range([height + abs_band.bottom, 0]),
                       "cdf": d3.scaleLinear()
                                .domain([0, 1 + abs_band.top*1/height])
                                .range([height, 0 - abs_band.top])};

// ***************************
// Coordinates for the curves
// ***************************

// x
var x_curve = d3.range(min_x, max_x + step_x, step_x);
var init_mu = 0;
var init_sigma = 1;

// *******************
// Plotting functions
// *******************

function draw_clip_path(holder, data, path, id) {
  var clipPath = holder.append("clipPath")
    .attr("id", id)
    .attr("class", "animated")
    .append("path")
    .datum(data)
    .attr("d", path);
  return(clipPath);

}

function draw_AUC(holder, coordinates, xscale, yscale, id, name, clip=null, animated="animated") {
  var rect = holder.append("rect")
    .attr("id", id)
    .attr("class", `${animated} AUC ${name}`)
  // Add left corner, width and height
  rect
    .attr("x", xscale(coordinates.x_min))
    .attr("y", yscale(coordinates.y_max))
    .attr("width", xscale(coordinates.x_max) - xscale(coordinates.x_min))
    .attr("height", yscale(coordinates.y_min) - yscale(coordinates.y_max))
  // Add clip path if specified
  if (clip != null) {
    rect.attr("clip-path", `url(${clip})`);
  }
  return(rect)
}

function draw_straight_line(holder, coordinates, xscale, yscale, id, param_name, line_type, animated="animated", clip=null) {
  var line = holder.append("line")
    .attr("id", id)
    .attr("class", `${animated} line ${param_name} ${line_type}`);
  // Add coordinates
  line
    .attr("x1", xscale(coordinates.x1))
    .attr("x2", xscale(coordinates.x2))
    .attr("y1", yscale(coordinates.y1))
    .attr("y2", yscale(coordinates.y2));
  // Add line width if it is AUC line:
  if (param_name == 'AUC' & line_type == 'AUC') {
    line.style("stroke-width", auc_line_width);
  }
  // Add clipPath if specified
  if (clip != null & clip != undefined) {
    line.attr("clip-path", `url(${clip})`);
  }
  return(line);
}

function draw_curve(holder, data, path, id, animated="animated") {
  var curve = holder.append("path")
    .attr("id", id)
    .attr("class", `${animated} gaussian_curve`)
    .datum(data)
    .attr("d", path);
  return(curve);
}

// ****************************************************
// Plotting the curves and auxiliary lines = main plot
// ****************************************************

// Create a group for each plot
svg.initialize_plot = function(y_shift, type) {
  pl = this.append("g")
    .attr("class", `plot plot-${type}`)
    .attr("transform", `translate(${padding.left}, ${y_shift})`)
    .style("stroke-linejoin", "round")
  return(pl)
}

var plot = {"pdf": svg.initialize_plot(padding.top, "pdf"),
            "cdf": svg.initialize_plot(padding.between/2+plot_height.pdf, "cdf")};

// Define the PDF and CDF curves
var curves = {"pdf": d3.line()
                      .curve(d3.curveBasis)
                        .x(function(d) { return xScale(d[0]); })
                        .y(function(d) { return yScale.pdf(d[1]); }),
              "cdf": d3.line()
                      .curve(d3.curveBasis)
                        .x(function(d) { return xScale(d[0]); })
                        .y(function(d) { return yScale.cdf(d[2]); })}


// calculate data and coordinates for all main elements of the plots
var df = get_data(x_curve, init_mu, init_sigma);
var curve_data = df.curve;
var clip_path_data = df.clip_path;
var AUC_coordinates = df.AUC;
var straight_line_coordinates = df.straight_line;

// define clip paths
var clips_for_straight_lines = {"pdf": {"sigma_left_aux_line": "band_clip_path_pdf",
                                        "sigma_right_aux_line": "band_clip_path_pdf",
                                        "mu_aux_line": "band_clip_path_pdf",
                                        "sigma_left_main_line": "exact_clip_path_pdf",
                                        "sigma_right_main_line": "exact_clip_path_pdf",
                                        "mu_main_line": "exact_clip_path_pdf"},
                                 "cdf": {"sigma_left_aux_line": "band_clip_path_cdf",
                                         "sigma_right_aux_line": "band_clip_path_cdf",
                                         "horizontal_sigma_left_aux_line": "band_clip_path_cdf",
                                         "horizontal_sigma_right_aux_line": "band_clip_path_cdf",
                                         "horizontal_mu_aux_line": "band_clip_path_cdf",
                                         "mu_aux_line": "band_clip_path_cdf",
                                         "sigma_left_main_line": "exact_clip_path_cdf",
                                         "sigma_right_main_line": "exact_clip_path_cdf",
                                         "mu_main_line": "exact_clip_path_cdf",
                                         "sigma_AUC_line": null}};
var clips_for_AUC = {"pdf": {"sigma_AUC": "exact_clip_path_pdf"}};

// define line classes
var line_classes = {"sigma_left_aux_line": {"param_name": "sigma",
                                            "line_type": "aux"},
                    "sigma_right_aux_line": {"param_name": "sigma",
                                             "line_type": "aux"},
                    "horizontal_sigma_left_aux_line": {"param_name": "sigma",
                                                       "line_type": "aux"},
                    "horizontal_sigma_right_aux_line": {"param_name": "sigma",
                                                        "line_type": "aux"},
                    "horizontal_mu_aux_line": {"param_name": "mu",
                                               "line_type": "aux"},
                    "mu_aux_line": {"param_name": "mu",
                                    "line_type": "aux"},
                    "sigma_left_main_line": {"param_name": "sigma",
                                             "line_type": "main"},
                    "sigma_right_main_line": {"param_name": "sigma",
                                              "line_type": "main"},
                    "mu_main_line": {"param_name": "mu",
                                     "line_type": "main"},
                    "sigma_AUC_line": {"param_name": "AUC",
                                      "line_type": "AUC"}};

// define y-scales
var y_scales_for_straight_lines = {"pdf": {"sigma_left_aux_line": oversize_yScale,
                                           "sigma_right_aux_line": oversize_yScale,
                                           "mu_aux_line": oversize_yScale,
                                           "sigma_left_main_line": yScale,
                                           "sigma_right_main_line": yScale,
                                           "mu_main_line": yScale},
                                   "cdf": {"sigma_left_aux_line": oversize_yScale,
                                           "sigma_right_aux_line": oversize_yScale,
                                           "horizontal_sigma_left_aux_line": oversize_yScale,
                                           "horizontal_sigma_right_aux_line": oversize_yScale,
                                           "horizontal_mu_aux_line": oversize_yScale,
                                           "mu_aux_line": oversize_yScale,
                                           "sigma_left_main_line": yScale,
                                           "sigma_right_main_line": yScale,
                                           "mu_main_line": yScale,
                                           "sigma_AUC_line": yScale}};

// Auxliary functions to draw elements
clip_path_aux = function(t, n) {
  draw_clip_path(holder = plot[t],
                 data = clip_path_data[n],
                 path = curves[t],
                 id = `${n}_${t}`);
};
curve_aux = function(t, n) {
  draw_curve(holder = plot[t],
             data = curve_data,
             path = curves[t],
             id = `${n}_gaussian_curve_${t}`,
             animated = n)
}
AUC_aux = function(t, n) {
  draw_AUC(holder = plot[t],
           coordinates = AUC_coordinates[t][n],
           xscale = xScale,
           yscale = yScale[t],
           id =`${n}_${t}`,
           name = `${n}`,
           clip = `#${clips_for_AUC[t][n]}`,
           animated="animated")
}
straight_line_aux = function(t, n) {
  var clip = clips_for_straight_lines[t][n];
  if (clips_for_straight_lines[t][n] != null & clips_for_straight_lines[t][n]!= undefined) {
    clip = `#${clip}`
  } else {
    clip = null
  }
  draw_straight_line(holder = plot[t],
                     coordinates = straight_line_coordinates[t][n],
                     xscale = xScale,
                     yscale = y_scales_for_straight_lines[t][n][t],
                     id = `${n}_${t}`,
                     param_name = line_classes[n].param_name,
                     line_type = line_classes[n].line_type,
                     animated = "animated",
                     clip = clip);
};


// clip paths
types.map(t => {
  var clip_names = Object.keys(clip_path_data);
  clip_names.map(n => clip_path_aux(t, n));
})
// Standard Gaussian curve
types.map(t => {
  curve_aux(t, "standard");
})
// AUC
Object.keys(AUC_coordinates).map(t => {
   var auc_names = Object.keys(AUC_coordinates[t]);
   auc_names.map(n => AUC_aux(t, n));
})
// mu lines
types.map(t => {
  var line_names = Object.keys(straight_line_coordinates[t]);
  line_names.map(n => straight_line_aux(t, n));
})
// Animated Gaussian curve
types.map(t => {
  curve_aux(t, "animated");
})

// *****
// Axes
// *****

// x-axis
var xAxis = d3.axisBottom().scale(xScale).ticks(9);
//y-axis
var yAxis = {"pdf": d3.axisLeft().scale(yScale.pdf).ticks(9),
             "cdf": d3.axisLeft().scale(yScale.cdf).ticks(9)}

// Append the axes
svg.append_axis = function(y_shift, axis) {
  this.append("g")
    .attr("class", "axis")
    .attr("transform", `translate(${padding.left}, ${y_shift})`)
    .call(axis)
}

// x-axis for PDF
svg.append_axis(padding.top + height, xAxis);
// x-axis for CDF
svg.append_axis(padding.between/2 + plot_height.pdf + height, xAxis);
// y-axis for PDF
svg.append_axis(padding.top, yAxis.pdf);
// y-axis for CDF
svg.append_axis(padding.between/2 + plot_height.pdf, yAxis.cdf);

// ************
// Axis labels
// ************

// methods to create and place KaTeX labels
class KaTeXlabel {
  constructor(fo, span, rotation, anchor_x, anchor_y, offset_x=0, offset_y=0, span_width = 0, span_height = 0) {
    this.fo = fo;
    this.span = span;
    this.rotation = rotation;
    this.anchor_x = anchor_x;
    this.anchor_y = anchor_y;
    this.offset_x = offset_x;
    this.offset_y = offset_y;
    this.span_width = span_width;
    this.span_height = span_height;
  }
  place_KaTeX(trans_x, trans_y) {

    var span_box = this.span.node().getBoundingClientRect(); // dimensions of the original span
    var phi = this.rotation*Math.PI/180; // rotation angle in radians

    // Save the untransformed box dimensions
    this.span_width = span_box.width;
    this.span_height = span_box.height;

    // Calculate the dimensions of the box after the rotation
    var w = Math.abs(span_box.width*Math.cos(phi) + span_box.height*Math.sin(phi));
    var h = Math.abs(span_box.width*Math.sin(phi) + span_box.height*Math.cos(phi));

    // Calculate the desired center of the box after the rotation
    var shift_x = {"W": w/2 + this.offset_x,
                   "C": 0,
                   "E": -w/2 - this.offset_x};
    var shift_y = {"N": h/2 + this.offset_y,
                   "C": 0,
                   "S": -h/2 - this.offset_y};
    var center_x = trans_x + shift_x[this.anchor_x];
    var center_y = trans_y + shift_y[this.anchor_y];


    // Place the span object
    if (navigator.userAgent.indexOf("Chrome") == -1 && navigator.userAgent.indexOf("Safari") != -1) {
      var svg_box = svg.node().getBoundingClientRect();
      this.span.style("left", center_x - span_box.width/2 + "px");
      this.span.style("top", center_y - span_box.height/2 +"px");
    } else {
      this.span.style("left", center_x - span_box.width/2 + "px");
      this.span.style("top", center_y - span_box.height/2 +"px");
    }


    // Rotate the object
    this.span.style("transform", `rotate(${this.rotation}deg)`)
    //this.span.style("transform-origin", `${span_box.width/2}px ${span_box.height/2}px`)
    this.span.style("transform-origin", `50% 50%`)

    // svg.append("line").style("stroke", "red").attr("x1", trans_x).attr("x2", trans_x).attr("y1", 0).attr("y2", plot_height.pdf + plot_height.cdf);
    // svg.append("line").style("stroke", "green").attr("x1", 0).attr("x2", plot_width).attr("y1", trans_y).attr("y2", trans_y);

  }

  move_KaTeX(trans_x, trans_y, tr_dur = 0) {

    var span_box = this.span.node().getBoundingClientRect(); // dimensions of the original span
    var phi = this.rotation*Math.PI/180; // rotation angle in radians

    // Calculate the dimensions of the box after the rotation
    var w = Math.abs(span_box.width*Math.cos(phi) + span_box.height*Math.sin(phi));
    var h = Math.abs(span_box.width*Math.sin(phi) + span_box.height*Math.cos(phi));

    // Calculate the desired center of the box after the rotation
    var shift_x = {"W": span_box.width/2 + this.offset_x,
                   "C": 0,
                   "E": -span_box.width/2 - this.offset_x};
    var shift_y = {"N": span_box.height/2 + this.offset_y,
                   "C": 0,
                   "S": -span_box.height/2 - this.offset_y};
    var center_x = trans_x + shift_x[this.anchor_x];
    var center_y = trans_y + shift_y[this.anchor_y];

    if (tr_dur > 0) {
      this.span
        .attr("transition", `left ${tr_dur}ms, top ${tr_dur}ms`);
    }

    // Place the span object
    this.span.style("left", center_x - this.span_width/2 + "px");
    this.span.style("top", center_y - this.span_height/2 +"px");

    if (navigator.userAgent.indexOf("Chrome") == -1 && navigator.userAgent.indexOf("Safari") != -1) { // if Safari:
      // trick Safari to "scroll" the window such that the labels actually move
      window.scrollBy(-1,1);
      window.scrollBy(1,-1);

    }

  }

  resize_KaTeX(trans_x, trans_y, anchor_x, anchor_y, offset_x=0, offset_y=0, tr_dur = 0) {
    // Get actual dimensions of the katex text (considering the baseline)

    var tex_dim = this.span.node().getBoundingClientRect()

    // calculate new position depending on anchors
    var shift_x = {"W": 0-this.offset_x,
                   "C": tex_dim.width/2,
                   "E": tex_dim.width+this.offset_x};
    var shift_y = {"N": 0-this.offset_y,
                   "C": tex_dim.height/2,
                   "S": tex_dim.height+this.offset_y};

    this.fo.attr("x", trans_x - shift_x[this.anchor_x]);
    this.fo.attr("y", trans_y - shift_y[this.anchor_y]);

    // align span with the foreignObject
    var fo_pos = this.fo.node().getBoundingClientRect()
    var span_pos = this.span.node().getBoundingClientRect()
    console.log(span_pos);
    console.log(fo_pos);
    var abs_x = fo_pos.x - span_pos.x;
    var abs_y = fo_pos.y - span_pos.y;

    this.span.style("left", abs_x+"px");
    this.span.style("top", abs_y+"px");

  }
}

svg.add_KaTeX = function(label, class_n, rotation = 0, anchor_x = "C", anchor_y = "C", offset_x = 0, offset_y = 0) {
  // rotation null if no rotation, otherwise angle between 0 and 360 degrees
  var fo = this
    .append("foreignObject")
      .style("position", "relative")
      .attr("width", "100%")
      .attr("height", "100%")
      .attr("x", 0)
      .attr("y", 0)
  var span = fo.append("xhtml:span")
    .style("position", "absolute")
    .attr("class", class_n)

  span.html(label) // attach label
  // for Safari use position fixed
  if (navigator.userAgent.indexOf("Chrome") == -1 && navigator.userAgent.indexOf("Safari") != -1) {
    span.style("position", "fixed")
  }
  var span_box = span.node().getBoundingClientRect();
  var new_label = new KaTeXlabel(fo, span, rotation, anchor_x, anchor_y, offset_x, offset_y, span_box.width, span_box.height);
  return(new_label)
}

// x-axis labels
const xAxisLabel = {"cdf": svg.add_KaTeX("$x$", "x-axis-label")};
// y-axis labels
const yAxisLabel = {"pdf": svg.add_KaTeX("Density $\\ p\\left(x\\mid \\mu, \\sigma^{2}\\right)$", "y-axis-label", rotation=270, anchor_x="E", anchor_y="C", offset_x=0, offset_y=0),
                    "cdf": svg.add_KaTeX("Probability $\\ P\\left(x\\mid \\mu, \\sigma^{2}\\right)$", "y-axis-label", rotation=270, anchor_x="E", anchor_y="C", offset_x=0, offset_y=0)};

// **********************
// Auxiliary line labels
// **********************

const muLabel = svg.add_KaTeX("$\\mu$", "mu-katex-label");
const sigmaLabel = {"left": svg.add_KaTeX("$\\mu-\\sigma$", "sigma-katex-label", rotation=0, anchor_x="E", anchor_y="C", offset_x=label_offset_x),
                    "right": svg.add_KaTeX("$\\mu+\\sigma$", "sigma-katex-label", rotation=0, anchor_x="W", anchor_y="C", offset_x=label_offset_x)};



// **********
// Animation
// **********

// Auxiliary functions to a single element
move_curve = function(data, t, tr_dur) {
  plot[t].select(`.animated.gaussian_curve`)
    .datum(data)
    .transition()
    .ease(transition_mode)
    .duration(tr_dur)
    .attr("d", curves[t]);
}
move_clip_path = function(data, t, n, tr_dur) {
  plot[t].select(`#${n}_${t}`)
    .select("path")
      .datum(data)
      .transition()
      .ease(transition_mode)
      .duration(tr_dur)
      .attr("d", curves[t]);
}
move_AUC = function(coordinates, t, n, tr_dur) {
  var a = plot[t].select(`#${n}_${t}`)
    .transition()
    .ease(transition_mode)
    .duration(tr_dur);
  // update all the coordinates
  a
    .attr("x", xScale(coordinates[t][n].x_min))
    .attr("y", yScale[t](coordinates[t][n].y_max))
    .attr("width", xScale(coordinates[t][n].x_max) - xScale(coordinates[t][n].x_min))
    .attr("height", yScale[t](coordinates[t][n].y_min) - yScale[t](coordinates[t][n].y_max))
}
move_straight_line = function(coordinates, t, n, tr_dur) {
  var l = plot[t].select(`#${n}_${t}.animated`)
    .transition()
    .ease(transition_mode)
    .duration(tr_dur)
  // update coordinates
  l
    .attr("x1", xScale(coordinates[t][n].x1))
    .attr("x2", xScale(coordinates[t][n].x2))
    .attr("y1", yScale[t](coordinates[t][n].y1))
    .attr("y2", yScale[t](coordinates[t][n].y2))
}


// Update the animated curves
function update_curves(mu, sigma, delta_mu, delta_sigma) {

  // new dataframes
  df = get_data(x_curve, mu, sigma);

  // transition_duration
  var tr_dur = transition_duration(delta_mu, delta_sigma)

  // update clip paths
  types.map(t => {
    var clip_names = Object.keys(clip_path_data);
    clip_names.map(n => move_clip_path(df.clip_path[n], t, n, tr_dur));
  })
  // update AUC
  Object.keys(AUC_coordinates).map(t => {
    var auc_names = Object.keys(AUC_coordinates[t]);
    auc_names.map(n => move_AUC(df.AUC, t, n, tr_dur));
  })
  // update straight lines
  types.map(t => {
    var line_names = Object.keys(straight_line_coordinates[t])
    line_names.map(n => move_straight_line(df.straight_line, t, n, tr_dur))
  })
  // update curves
  types.map(t => {
    move_curve(df.curve, t, tr_dur)
  })

  muLabel.move_KaTeX(padding.left + xScale(mu), label_position_y, tr_dur = tr_dur);
  sigmaLabel.left.move_KaTeX(padding.left + xScale(mu - sigma), label_position_y, tr_dur = tr_dur);
  sigmaLabel.right.move_KaTeX(padding.left + xScale(mu + sigma), label_position_y, tr_dur = tr_dur);

}

// **************
// Slider events
// **************

// Listen on mu-slider
slider_mu.slider.on("change", function(){
  mu = parseFloat(this.value)
  delta_mu = Math.abs(mu - slider_mu.mu());
  slider_mu.slider.attr("value", mu)
  slider_mu.value.html(repr_value(mu))
  sigma = slider_sigma.sigma()
  update_curves(mu, sigma, delta_mu, 0)

});
// Listen on sigma-slider
slider_sigma.slider.on("change", function(){
  sigma = parseFloat(this.value)
  delta_sigma = Math.abs(sigma - slider_sigma.sigma());
  slider_sigma.slider.attr("value", sigma)
  slider_sigma.value.html(repr_value(sigma))
  mu = slider_mu.mu()
  update_curves(mu, sigma, 0, delta_sigma)
});

// ************************
// KaTeX placing functions
// ************************

function place_labels(event) {
  xAxisLabel.cdf.place_KaTeX(padding.left+width/2, plot_height.pdf + plot_height.cdf - padding.bottom/2);
  yAxisLabel.pdf.place_KaTeX(padding.left/2, padding.top+height/2);
  yAxisLabel.cdf.place_KaTeX(padding.left/2, plot_height.pdf + padding.between/2 + height/2);
  muLabel.place_KaTeX(padding.left + xScale(slider_mu.mu()), label_position_y);
  sigmaLabel.left.place_KaTeX(padding.left + xScale(slider_mu.mu() - slider_sigma.sigma()), label_position_y);
  sigmaLabel.right.place_KaTeX(padding.left + xScale(slider_mu.mu() + slider_sigma.sigma()), label_position_y);
}

function resize_labels(event) {
  yAxisLabel.pdf.resize_KaTeX(padding.left/2, padding.top+height/2, "E", "C");
  yAxisLabel.cdf.resize_KaTeX(padding.left/2, plot_height.pdf + padding.between/2 + height/2, "E", "C");
  sigmaLabel.left.resize_KaTeX(padding.left + xScale(slider_mu.mu() - slider_sigma.sigma()), label_position_y, "E", "C", offset_x=label_offset_x);
}

window.addEventListener("load", place_labels);