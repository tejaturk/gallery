# Normal distribution

A repository accompanying the following plot on my personal website: https://www.tejaturk.com/gallery/normal_distribution/

It includes an `html` along with a `js` script and a `css` file, which provide the code to produce the plot and its styling.